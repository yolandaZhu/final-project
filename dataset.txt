The data set is autism prevalence studies data from below website:
https://catalog.data.gov/dataset/autism-prevalence-studies

I plan to find the trend for "number of publications" per year, the trend for 'ASD Prevalence Estimate per 1,000', and
'Male:Female Sex Ratio' by year.
