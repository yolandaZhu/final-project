"""
author: Yingchi Zhu
date: 4/23
section leader:
ISTA131 Final Project
This module reads the data from the autism prevalence studies csv file and make 3 plots, respectively, for number of
publications by year, sex ratio for 3 ranges of years, and prevalence by year.
"""
import math
import numpy as np
import pandas as pd
from scipy.stats import linregress
import matplotlib.pyplot as plt


def get_ols_parameters(x, y):
    """
    Get the ols parameters for the input series and return the parameters as a list.
    """
    regression_model = linregress(x, y)
    return [regression_model.slope, regression_model.intercept, math.pow(regression_model.rvalue, 2),
            regression_model.pvalue]


def read_data(fp):
    """
    Reads data from the csv file specified in the fp and return the data frame.
    """
    return pd.read_csv(fp)


def extract_publication(df):
    """
    Takes a data frame containing all the data and returns the series for the column 'Year Published'.
    """
    return df.loc[:, 'Year Published']


def plot_publication(s):
    """
    Takes a series for the year published for the studies. make a scatter plot and its linear regression for publication
    count vs year.
    """
    fig = plt.figure()
    fig.patch.set_facecolor('rosybrown')
    ax = plt.gca()

    ax.spines['top'].set_color('lime')
    ax.spines['left'].set_color('lime')
    ax.spines['bottom'].set_color('lime')
    ax.spines['right'].set_color('lime')

    ax.tick_params(axis='x', colors='lime', labelsize=15)
    ax.tick_params(axis='y', colors='lime', labelsize=15)
    plt.yticks(np.arange(0, 16, step=3))

    data = s.value_counts()
    data = data.drop(labels=[v for v in data.index if v < 1990])
    ax.scatter(x=data.index, y=data, label="number of publications", s=10)

    slope, intercept, rvalue2, pvalue = get_ols_parameters(data.index, data.values)
    year_range = range(1990, 2021)
    pd.Series(data=[slope * year + intercept for year in year_range], index=year_range).plot(
        label="linear regression for #publications", color='g')

    ax.set_facecolor("#f0ffff")

    plt.legend(fontsize=12)
    plt.xlabel('Year', fontsize=18, color='lime')
    plt.ylabel('Number of Publications', fontsize=18, color='lime')
    plt.title('Number of ASD Study Publications', fontsize=20, color='lime')
    plt.tight_layout()


def extract_sex_ratio(df):
    """
    Takes a data frame containing all the data and returns 3 series. Each of them is for 'Male:Female Sex Ratio'. The
    first series is for data before year 2000; the second series is for data between year 2001 and 2010; the third is
    for data between year 2011 and 2020.
    """
    df1 = df[df['Year Published'] <= 2000]

    df2 = df[df['Year Published'] >= 2001]
    df2 = df2[df2['Year Published'] <= 2010]

    df3 = df[df['Year Published'] >= 2011]
    df3 = df3[df3['Year Published'] <= 2020]

    s_before_2000 = df1.loc[:, 'Male:Female Sex Ratio'].dropna()
    s_2001_2010 = df2.loc[:, 'Male:Female Sex Ratio'].dropna()
    s_2011_2020 = df3.loc[:, 'Male:Female Sex Ratio'].dropna()

    return s_before_2000, s_2001_2010, s_2011_2020


def plot_sex_ratio(s_before_2000, s_2001_2010, s_2011_2020):
    """
    Takes the series for different year ranges and make styled box plots based on the series.
    """
    fig = plt.figure()
    fig.patch.set_facecolor('#e0e0e0')

    ax = plt.gca()
    ax.boxplot(s_before_2000, positions=[0])
    ax.boxplot(s_2001_2010, positions=[1])
    ax.boxplot(s_2011_2020, positions=[2])

    ax.spines['top'].set_color('r')
    ax.spines['left'].set_color('r')
    ax.spines['bottom'].set_color('r')
    ax.spines['right'].set_color('r')

    ax.tick_params(axis='x', colors='r', labelsize=15)
    ax.tick_params(axis='y', colors='r', labelsize=15)
    plt.yticks(np.arange(0, 11, step=1))

    ax.set_facecolor("#fff0f0")
    ax.set_xticklabels(['1966 - 2000', '2001 - 2010', '2011 - 2020'], rotation=0, fontsize=15, color='red')

    plt.xlabel('Range of Years', fontsize=18, color='r')
    plt.ylabel('Male:Female Sex Ratio', fontsize=18, color='r')
    plt.title('Sex Ratio for Different Range of Years', fontsize=20, color='r')
    plt.tight_layout()


def extract_prevalence(df):
    """
    Extract the data frame for 'ASD Prevalence Estimate per 1000' for the data in year ranging from 1990 to 2020. We
    also maintain the 'Year Published' data.
    """
    df = df[df['Year Published'] >= 1990]
    return df.loc[:, ['ASD Prevalence Estimate per 1,000', 'Year Published']].dropna()


def plot_prevalence(df):
    """
    Based on the data frame returned by extract_prevalence(df), make a scatter plot for prevalence and its linear
    regression.
    """
    fig = plt.figure()
    fig.patch.set_facecolor('lemonchiffon')
    ax = plt.gca()

    ax.spines['top'].set_color('navy')
    ax.spines['left'].set_color('navy')
    ax.spines['bottom'].set_color('navy')
    ax.spines['right'].set_color('navy')

    ax.tick_params(axis='x', colors='navy', labelsize=15)
    ax.tick_params(axis='y', colors='navy', labelsize=15)
    plt.yticks(np.arange(0, 45, step=10))

    x = df.loc[:, 'Year Published']
    y = df.loc[:, 'ASD Prevalence Estimate per 1,000']

    ax.scatter(x=x, y=y, label="study", s=10, color='navy')

    slope, intercept, rvalue2, pvalue = get_ols_parameters(x, y)
    year_range = range(1990, 2021)
    pd.Series(data=[slope * year + intercept for year in year_range], index=year_range).plot(
        label="linear regression for ASD prevalence", color='r')

    ax.set_facecolor("#f0f0ff")

    plt.legend(fontsize=12)
    plt.xlabel('Year', fontsize=18, color='navy')
    plt.ylabel('ASD Prevalence per 1000 People', fontsize=18, color='navy')
    plt.title('ASD Prevalence by Years', fontsize=20, color='navy')
    plt.tight_layout()


def main():
    df = read_data('autism_prevalence_studies.csv')

    year_published = extract_publication(df)
    plot_publication(year_published)
    plt.show(block=False)

    s_before_2000, s_2001_2010, s_2011_2020 = extract_sex_ratio(df)
    plot_sex_ratio(s_before_2000, s_2001_2010, s_2011_2020)
    plt.show(block=False)

    s_prevalence = extract_prevalence(df)
    plot_prevalence(s_prevalence)
    plt.show()


if __name__ == '__main__':
    main()
